/**
 * \file skeleton.c
 * \brief Basic parsing options skeleton.
 * \author Pierre L. <pierre1.leroy@orange.com>
 * \version 0.1
 * \date 10 septembre 2016
 *
 * Basic parsing options skeleton exemple c file.
 */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>

#include<getopt.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>

#include<grp.h>
#include<pwd.h>
#include<time.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<dirent.h>


#define STDOUT 1
#define STDERR 2

#define MAX_PATH_LENGTH 4096


#define USAGE_SYNTAX "[OPTIONS] -e EXERCICE -i INPUT -o OUTPUT"
#define USAGE_PARAMS "OPTIONS:\n\
  -e, --exercice EXERCICE : [c, r, l]\n\
  -i, --input  INPUT_FILE  : input file\n\
  -o, --output OUTPUT_FILE : output file\n\
***\n\
  -v, --verbose : enable *verbose* mode\n\
  -h, --help    : display this help\n\
"

/**
 * Procedure which displays binary usage
 * by printing on stdout all available options
 *
 * \return void
 */
void print_usage(char* bin_name)
{
  dprintf(1, "USAGE: %s %s\n\n%s\n", bin_name, USAGE_SYNTAX, USAGE_PARAMS);
}


/**
 * Procedure checks if variable must be free
 * (check: ptr != NULL)
 *
 * \param void* to_free pointer to an allocated mem
 * \see man 3 free
 * \return void
 */
void free_if_needed(void* to_free)
{
  if (to_free != NULL) free(to_free);  
}


/**
 *
 * \see man 3 strndup
 * \see man 3 perror
 * \return
 */
char* dup_optarg_str()
{
  char* str = NULL;

  if (optarg != NULL)
  {
    str = strndup(optarg, MAX_PATH_LENGTH);
    
    // Checking if ERRNO is set
    if (str == NULL) 
      perror(strerror(errno));
  }

  return str;
}

int copy_file(char* input, char* output)
{
  int input_describer = open(input, O_RDONLY);
  int output_describer = open(output, O_RDWR|O_CREAT, 00744);

  // Check if INPUT_FILE and OUTPUT_FILE are files
  if(input_describer == -1 || output_describer == -1)
  {
    dprintf(STDERR, "Bad usage ! Not a file\n");
    exit(EXIT_FAILURE);
  }
  
  char *buffer = NULL;

  off_t offset = lseek(input_describer, 0, SEEK_END);
  if(offset == -1)
  {
    dprintf(STDERR, "File is empty\n");
    exit(EXIT_FAILURE);
  }
  buffer = malloc(sizeof(char)* offset + 1);
  lseek(input_describer, 0, SEEK_SET);

  ssize_t fileLength = read(input_describer, buffer, offset);

  if(fileLength == -1)
  {
    dprintf(STDERR, "Read failed");
    exit(EXIT_FAILURE);
  }

  ssize_t lengthWritten = write(output_describer, buffer, fileLength);

  if(fileLength == -1)
  {
    dprintf(STDERR, "Write failed");
    exit(EXIT_FAILURE);
  }

  printf("File copied %s -> %s.\ncontent : %s", input, output, buffer); 

  close(input_describer);
  close(output_describer);
  free(buffer);
  return 0;
}

/**
  * Function for the reversing of a file
**/

int reverse_file(char* input, char* output)
{
  int input_describer = open(input, O_RDONLY);
  int output_describer = open(output, O_RDWR|O_CREAT, 00744);

  // Check if INPUT_FILE and OUTPUT_FILE are files
  if(input_describer == -1 || output_describer == -1)
  {
    dprintf(STDERR, "Bad usage ! Not a file\n");
    exit(EXIT_FAILURE);
  }

  char buffer[1];

  off_t file_length = lseek(input_describer, 0, SEEK_END);

  if(file_length == -1)
  {
    dprintf(STDERR, "Input file is empty");
    exit(EXIT_FAILURE);
  }
  lseek(input_describer, -1, SEEK_END);
  lseek(output_describer, 0, SEEK_SET);

  while(file_length > 0)
  {
    ssize_t length_read = read(input_describer, buffer, 1);

    lseek(output_describer, 0, SEEK_CUR);
    write(output_describer, buffer, length_read);

    lseek(input_describer, -2, SEEK_CUR);
    file_length--;
  }

  off_t output_length = lseek(output_describer, 0, SEEK_END);
  char* bufferOutput = malloc(sizeof(char)* output_length);
  lseek(output_describer, 0, SEEK_SET);

  read(output_describer, bufferOutput, output_length);

  printf("File copied %s -> %s.\ncontent:\n%s\n", input, output, bufferOutput); 

  free(bufferOutput);
  close(input_describer);
  close(output_describer);
  return 0;
}


int do_ls(char* input, char* output)
{
  char* current_dir = NULL;
  DIR* directory = NULL;
  struct dirent *dptr = NULL;

  int output_describer = open(output, O_RDWR|O_CREAT|O_TRUNC, 00744);

  // Check OUTPUT_FILE is created
  if(output_describer == -1)
  {
    dprintf(STDERR, "Bad usage ! Not a file\n");
    exit(EXIT_FAILURE);
  }

  current_dir = input;


  directory = opendir(current_dir);

  if(directory){
    printf("ls on %s, printed in file %s\n",current_dir, output);
  }
  else if (ENOENT == errno)
  {
    perror("ERROR : not a directory");
    return -1;
  }
  else 
  {
    perror("ERROR : opendir failed for unknown reason");
    return -1;
  }

  char* content = (char*) malloc(5000* sizeof(char));
  sprintf(content, "%-30s %-15s %-15s %-15s %-15s %-15s\n", "Filename", "Permissions", "Owner", "Group", "Size", "Last Update");
  
  int dfd = dirfd(directory);
  while((dptr = readdir(directory)) != NULL)
  {
    struct stat stats;
    struct passwd *pwd;
    struct group *grp;
    struct tm *time;
    
    if(fstatat(dfd, dptr->d_name, &stats, 0) == -1)
    {
      printf("%d\n", dfd);
      perror("ERROR: erreur dans stat");
      continue;
    }
    printf("d_name : %s\n", dptr->d_name);
    // PASSWORD
    if((pwd = getpwuid(stats.st_uid)) == NULL)
    {
      perror("ERROR : erreur dans password");
      continue;
    }

    // GROUP
    if((grp = getgrgid(stats.st_gid)) == NULL)
    {
      perror("ERROR:  erreur dans group");
      continue;
    }

    // TIME
    if((time = localtime(&stats.st_mtim)) == NULL)
    {
      perror("ERROR: erreur dans time");
      continue;
    }
    char* temp = (char*)malloc(400 * sizeof(char));
    sprintf(temp, "%-30s %-1s%-1s%-1s%-1s%-1s%-1s%-1s%-1s%-1s%-6s %-15s %-15s %-15ld %02d%02d%04d %02d:%02d:%02d\n",
    dptr->d_name,
    (S_ISDIR(stats.st_mode)) ? "d" : "-",
    (stats.st_mode & S_IRUSR) ? "r" : "-",
    (stats.st_mode & S_IWUSR) ? "w" : "-",
    (stats.st_mode & S_IXUSR) ? "x" : "-",
    (stats.st_mode & S_IRGRP) ? "r" : "-",
    (stats.st_mode & S_IWGRP) ? "w" : "-",
    (stats.st_mode & S_IXGRP) ? "x" : "-",
    (stats.st_mode & S_IROTH) ? "r" : "-",
    (stats.st_mode & S_IWOTH) ? "w" : "-",
    (stats.st_mode & S_IXOTH) ? "x" : "-",
    (pwd->pw_name),
    (grp->gr_name),
    (stats.st_size),
    time->tm_mday,
    time->tm_mon + 1,
    time->tm_year + 1900,
    time->tm_hour,
    time->tm_min,
    time->tm_sec
    );
    strcat(content, temp);
    free(temp);
  }

  printf("%s", content);
  write(output_describer, content, strlen(content));

  free(content);

  return EXIT_SUCCESS;
}
/**
 * Binary options declaration
 * (must end with {0,0,0,0})
 *
 * \see man 3 getopt_long or getopt
 * \see struct option definition
 */
static struct option binary_opts[] = 
{
  { "help",      no_argument,       0, 'h' },
  { "verbose",   no_argument,       0, 'v' },
  { "input",     required_argument, 0, 'i' },
  { "output",    required_argument, 0, 'o' },
  { "exercices", required_argument, 0, 'e' },
  { 0,         0,                   0,  0  } 
};

/**
 * Binary options string
 * (linked to optionn declaration)
 *
 * \see man 3 getopt_long or getopt
 */ 
const char* binary_optstr = "hvi:o:e:";



/**
 * Binary main loop
 *
 * \return 1 if it exit successfully 
 */
int main(int argc, char** argv)
{
  /**
   * Binary variables
   * (could be defined in a structure)
   */
  short int is_verbose_mode = 0;
  char* bin_input_param = NULL;
  char* bin_output_param = NULL;
  char* temp = NULL;
  char bin_exercice_param = 0;

  // Parsing options
  int opt = -1;
  int opt_idx = -1;

  while ((opt = getopt_long(argc, argv, binary_optstr, binary_opts, &opt_idx)) != -1)
  {
    switch (opt)
    {
      case 'i':
        //input param
        if (optarg)
        {
          bin_input_param = dup_optarg_str();         
        }
        break;
      case 'o':
        //output param
        if (optarg)
        {
          bin_output_param = dup_optarg_str();
        }
        break;
      case 'v':
        //verbose mode
        is_verbose_mode = 1;
        break;
      case 'h':
        print_usage(argv[0]);

        free_if_needed(bin_input_param);
        free_if_needed(bin_output_param);
 
        exit(EXIT_SUCCESS);
      case 'e':
        temp = dup_optarg_str();
        bin_exercice_param = temp[0];
        free(temp);
      default :
        break;
    }
  } 

  /**
   * Checking binary requirements
   * (could defined in a separate function)
   */
  if (bin_exercice_param == 0 || bin_input_param == NULL || bin_output_param == NULL)
  {
    dprintf(STDERR, "Bad usage! See HELP [--help|-h]\n");

    // Freeing allocated data
    free_if_needed(bin_input_param);
    free_if_needed(bin_output_param);
    // Exiting with a failure ERROR CODE (== 1)
    exit(EXIT_FAILURE);
  }


  // Printing params
  dprintf(1, "** PARAMS **\n%-8s: %c\n%-8s: %s\n%-8s: %s\n%-8s: %d\n",
          "exercice", bin_exercice_param, 
          "input",   bin_input_param, 
          "output",  bin_output_param, 
          "verbose", is_verbose_mode);

  // Business logic must be implemented at this point

  if(bin_exercice_param == 'c')
  {
    int returnValue = copy_file(bin_input_param, bin_output_param);
    if(returnValue != 0)
    {
      dprintf(STDERR, "copy_file failed.\n");
    }
  }
  else if(bin_exercice_param == 'r')
  {
    int returnValue = reverse_file(bin_input_param, bin_output_param);
    if(returnValue != 0)
    {
      dprintf(STDERR, "reverse_file failed.\n");
    }
  }
  else if(bin_exercice_param == 'l')
  {
    int returnValue = do_ls(bin_input_param, bin_output_param);
    if(returnValue != 0)
    {
      dprintf(STDERR, "reverse_file failed.\n");
    }
  }
  else
  {
    dprintf(STDERR, "Bad usage ! See HELP [--help|_h]\n");
    free_if_needed(bin_input_param);
    free_if_needed(bin_output_param);
    exit(EXIT_FAILURE);
  }
  


  // Freeing allocated data
  free_if_needed(bin_input_param);
  free_if_needed(bin_output_param);


  return EXIT_SUCCESS;
}
